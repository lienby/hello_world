///create_functions(index)
var i = argument0
var farray = global.kmodFunctions[i]

instance_destroy(obj_select_kernel_function);

var len = array_length_1d(farray) -1;

for(i2 = 0; i2 <= len; i2++)
{
    var obj = instance_create(336,64+(16*i2),obj_select_kernel_function);
    obj.function = farray[i2];
    obj.index = i2;
    obj.mindex = i;
}
