///random_choice(array)

var len = array_length_1d(argument0) -1;
var ran = irandom_range(0,len);
return argument0[ran];
