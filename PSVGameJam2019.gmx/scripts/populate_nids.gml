libraries[0] = "SceUlobjMgr";
libraries[1] = "SceFios2Kernel";
libraries[2] = "SceFios2Kernel02";
libraries[3] = "SceUsbdForDriver";
libraries[4] = "SceUsbdForUser";
libraries[5] = "SceGxm";
libraries[6] = "SceCompat";
libraries[7] = "ScePgf";
libraries[8] = "SceNpBasic";
libraries[9] = "SceBbmc";
libraries[10] = "SceMsifForDriver";
libraries[11] = "SceNgsInternal";
libraries[12] = "ScePerf";
libraries[13] = "SceHid";
libraries[14] = "SceAppUtilExt";
libraries[15] = "SceAppUtilCache";
libraries[16] = "SceAppUtil";
libraries[17] = "SceAppUtilBook";
libraries[18] = "SceNpManager";
libraries[19] = "SceRtc";
libraries[20] = "SceRtcForDriver";
libraries[21] = "SceRtcUser";
libraries[22] = "SceOledForDriver";
libraries[23] = "ScePspnetAdhoc";
libraries[24] = "SceErrorUser";
libraries[25] = "SceDrmBridgeUser";
libraries[26] = "SceNpTus";
libraries[27] = "SceIofilemgr";
libraries[28] = "SceJpegEncUser";
libraries[29] = "SceSharedFb";
libraries[30] = "SceAppMgr";
libraries[31] = "SceAppMgrUser";
libraries[32] = "SceHttp";
libraries[33] = "SceLedForDriver";
libraries[34] = "ScePower";
libraries[35] = "SceVoice";
libraries[36] = "SceGameUpdate";
libraries[37] = "SceDbg";
libraries[38] = "SceAudiodecUser";
libraries[39] = "SceMotionDev";
libraries[40] = "SceGps";
libraries[41] = "SceBt";
libraries[42] = "SceBtForDriver";
libraries[43] = "SceNetCtl";
libraries[44] = "SceCommonDialog";
libraries[45] = "SceSblACMgr";
libraries[46] = "SceVideoExport";
libraries[47] = "SceDTrace";
libraries[48] = "SceMotion";
libraries[49] = "SceRazorHud";
libraries[50] = "SceMusicExport";
libraries[51] = "SceNpActivity";
libraries[52] = "SceSblAimgr";
libraries[53] = "SceSblRng";
libraries[54] = "SceSblDmac5Mgr";
libraries[55] = "SceSblQafMgr";
libraries[56] = "SceUsbstorVStor";
libraries[57] = "SceProcessmgr";
libraries[58] = "SceLibMonoBridge";
libraries[59] = "SceIme";
libraries[60] = "SceShutterSound";
libraries[61] = "SceNpMatching2";
libraries[62] = "SceSblGcAuthMgr";
libraries[63] = "SceNgs";
libraries[64] = "SceCtrl";
libraries[65] = "SceCtrlForDriver";
libraries[66] = "SceLiveAreaUtil";
libraries[67] = "SceDeci4pUserp";
libraries[68] = "SceNpUtility";
libraries[69] = "SceRazorCapture";
libraries[70] = "ScePhotoExport";
libraries[71] = "SceTouch";
libraries[72] = "SceSblUtMgr";
libraries[73] = "SceSblPmMgr";
libraries[74] = "SceSblLicMgr";
libraries[75] = "SceSblRtcMgr";
libraries[76] = "ScePvf";
libraries[77] = "SceSas";
libraries[78] = "SceVideodecUser";
libraries[79] = "SceCamera";
libraries[80] = "SceLibm";
libraries[81] = "SceLibc";
libraries[82] = "SceLibstdcxx";
libraries[83] = "SceVshBridge";
libraries[84] = "SceDrmBridge";
libraries[85] = "SceNpDrmPackage";
libraries[86] = "SceNpDrm";
libraries[87] = "ScePsmDrm";
libraries[88] = "SceDisplay";
libraries[89] = "SceDisplayUser";
libraries[90] = "SceIftuForDriver";
libraries[91] = "SceDsiForDriver";
libraries[92] = "SceI2cForDriver";
libraries[93] = "SceGpioForDriver";
libraries[94] = "SceModulemgr";
libraries[95] = "SceBacktrace";
libraries[96] = "SceWlan";
libraries[97] = "SceNpScore";
libraries[98] = "ScePamgr";
libraries[99] = "SceSmart";
libraries[100] = "SceCoredump";
libraries[101] = "SceFace";
libraries[102] = "SceUdcd";
libraries[103] = "SceUdcdForDriver";
libraries[104] = "SceAvcodec";
libraries[105] = "SceAtrac";
libraries[106] = "SceLsdb";
libraries[107] = "SceLcdForDriver";
libraries[108] = "SceNpTrophy";
libraries[109] = "SceNpCommon";
libraries[110] = "SceClipboard";
libraries[111] = "SceScreenShot";
libraries[112] = "SceShellUtil";
libraries[113] = "SceNpCommerce2";
libraries[114] = "SceNpMessage";
libraries[115] = "SceMtpIf";
libraries[116] = "SceNpSignaling";
libraries[117] = "SceSysmodule";
libraries[118] = "SceVoiceQoS";
libraries[119] = "SceNpWebApi";
libraries[120] = "SceAvPlayer";
libraries[121] = "SceLibKernel";
libraries[122] = "SceLibRng";
libraries[123] = "SceDebugLed";
libraries[124] = "SceDipsw";
libraries[125] = "SceCpu";
libraries[126] = "SceThreadmgr";
libraries[127] = "SceLibJson";
libraries[128] = "SceDmacmgr";
libraries[129] = "SceUsbSerial";
libraries[130] = "SceUlt";
libraries[131] = "SceAVConfig";
libraries[132] = "SceAudioIn";
libraries[133] = "ScePafStdc";
libraries[134] = "ScePafWidget";
libraries[135] = "ScePafResource";
libraries[136] = "ScePafMisc";
libraries[137] = "SceFios2User";
libraries[138] = "SceFios2";
libraries[139] = "SceError";
libraries[140] = "SceLibXml";
libraries[141] = "SceLibLocation";
libraries[142] = "SceGpuEs4ForUser";
libraries[143] = "SceNet";
libraries[144] = "SceAudio";
libraries[145] = "SceUsbServ";
libraries[146] = "SceAudioencUser";
libraries[147] = "ScePromoterUtil";
libraries[148] = "SceSulpha";
libraries[149] = "SceNpSnsFacebook";
libraries[150] = "SceSqlite";
libraries[151] = "SceSdifForDriver";
libraries[152] = "SceStdio";
libraries[153] = "SceLibRudp";
libraries[154] = "SceRegMgrForGame";
libraries[155] = "SceRegMgrService";
libraries[156] = "SceRegMgr";
libraries[157] = "SceRegMgrForSDK";
libraries[158] = "SceNearUtil";
libraries[159] = "SceFiber";
libraries[160] = "SceVideoencUser";
libraries[161] = "SceSsl";
libraries[162] = "SceJpegUser";
libraries[163] = "SceUsbPspcm";
libraries[164] = "SceUartForKernel";
libraries[165] = "SceSysmem";
libraries[166] = "SceCpuForDriver";
libraries[167] = "SceCpuForKernel";
libraries[168] = "SceSystemGesture";
libraries[169] = "SceHandwriting";

bytes[0] = "0";
bytes[1] = "1";
bytes[2] = "2";
bytes[3] = "3";
bytes[4] = "4";
bytes[5] = "5";
bytes[6] = "6";
bytes[7] = "7";
bytes[8] = "8";
bytes[9] = "9";
bytes[10] = "A";
bytes[11] = "B";
bytes[12] = "C";
bytes[13] = "D";
bytes[14] = "E";
bytes[15] = "F";

libsUsed[0] = -1
libsUsedIndex = 0;

blacklist[0] = -1;
blacklistIndex = 0;


for(var i = 0; i <= 30; i++)
{
    var libindex = irandom_range(0,169);
    while(array_contains(libsUsed,libraries[libindex]))
    {
        var libindex = irandom_range(0,169);
    }
    
    libsUsed[libsUsedIndex] = libraries[libindex];
    libsUsedIndex += 1;
}

for(var i = 0; i <= 30; i++)
{
    var obj = instance_create(208,26+(16*i),obj_select_port_tai_nid);
    obj.lib = libsUsed[i];
    obj.nid = "0x"
    repeat(4)
    {
        var b16Index = irandom_range(0,15);
        obj.nid += bytes[b16Index]
    }
}

for(var i = 0; i<=30; i++)
{
    var obj = instance_create(696,26+(16*i),obj_select_port_tai_code);
    libIndex = irandom_range(0,30);
    while(array_contains(blacklist,libIndex))
    {
        libIndex = irandom_range(0,30);
    }
    
    blacklist[blacklistIndex] = libIndex;
    blacklistIndex += 1;
    
    obj.lib = libsUsed[libIndex];
    obj.nid = "0x"
    repeat(4)
    {
        var b16Index = irandom_range(0,15);
        obj.nid += bytes[b16Index]
    }
}


