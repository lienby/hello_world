///get_asm(instructionCount, gadgetCount)

// Some ARM Instructions :D
Memonics[0] = "ADC"
Memonics[1] = "ADD"
Memonics[2] = "AND"
Memonics[3] = "B"
Memonics[4] = "BIC"
Memonics[5] = "BL"
Memonics[6] = "BX"
Memonics[7] = "CDP"
Memonics[8] = "CMN"
Memonics[9] = "CMP"
Memonics[10] = "EOR"
Memonics[11] = "LDC"
Memonics[12] = "LDM"
Memonics[13] = "LDR"
Memonics[14] = "MCR"
Memonics[15] = "MLA"
Memonics[16] = "MOV"
Memonics[17] = "MRC"
Memonics[18] = "MRS"
Memonics[19] = "MSR"
Memonics[20] = "MUL"
Memonics[21] = "MVN"
Memonics[22] = "ORR"
Memonics[23] = "RSB"
Memonics[24] = "RSC"
Memonics[25] = "SBC"
Memonics[26] = "STC"
Memonics[27] = "STM"
Memonics[28] = "STR"
Memonics[29] = "SUB"
Memonics[30] = "SWI"
Memonics[31] = "SWP"
Memonics[32] = "TEQ"
Memonics[33] = "TST"

//ARM Registers 

Registers[0] = "R0"
Registers[1] = "R1"
Registers[2] = "R2"
Registers[3] = "R3"
Registers[4] = "R4"
Registers[5] = "R5"
Registers[6] = "FP"
Registers[7] = "IP"
Registers[8] = "SP"
Registers[9] = "LR"
Registers[10] = "PC"
Registers[11] = "CSPR"
Registers[12] = "\#0"
Registers[13] = "\#1"
Registers[14] = "\#2"
Registers[15] = "\#3"
Registers[16] = "\#4"
Registers[17] = "\#5"
Registers[18] = "\#6"
Registers[19] = "\#7"
Registers[20] = "\#8"
Registers[21] = "\#9"
Registers[22] = "\#0"
Registers[23] = "R6"
Registers[24] = "R7"
Registers[25] = "R8"
Registers[26] = "R9"
Registers[27] = "R10"

instructionCount = argument0;
gadgetCount = argument1;

// Build an array of instructions 

for(i = 0; i <= instructionCount; i++)
{
    var lenMemonics = array_length_1d(Memonics) -1;
    var lenRegisters = array_length_1d(Registers) -1;
    
    var ran1 = irandom_range(0,lenMemonics);
    var ran2 = irandom_range(0,lenRegisters);
    var fullGadget = Memonics[ran1] +" "+Registers[ran2];
    
    if(irandom_range(1,100) >=50) //Determine if it should have 2 arguments
    {
        var ran3 = irandom_range(0,lenRegisters);
        fullGadget += ", "+Registers[ran3]
    }
    
    fullGadget += "#"
    
    global.InstructList[i] = fullGadget;
}

// Build an array of gadgets 

for(i = 0; i <= gadgetCount; i++)
{
    var lenMemonics = array_length_1d(Memonics) -1;
    var lenRegisters = array_length_1d(Registers) -1;
    
    var ran1 = irandom_range(0,lenMemonics);
    var ran2 = irandom_range(0,lenRegisters);
    var fullGadget = Memonics[ran1] +" "+Registers[ran2];
    
    if(irandom_range(0,1)) //Determine if it should have 2 arguments
    {
        var ran3 = irandom_range(0,lenRegisters);
        fullGadget += ", "+Registers[ran3]
    }
    
    fullGadget += "#"
    
    global.GadgetList[i] = fullGadget;
}


numReplaced = 0;
blacklist[0] = -1;

// Create blacklisted index table

for(i = 0; i <= gadgetCount; i++)
{
    var gadget = global.GadgetList[i];

    if(array_contains(global.InstructList,gadget))
    {
        var index = array_index(global.InstructList,gadget)
        if(!array_contains(blacklist,index))
            {
            show_debug_message("correct result found blacklisting! " +string(index))
            blacklist[numReplaced] = index;
            numReplaced ++;
        }
    }
}

// Force instructions to have every gadget *somewhere*


for(i = 0; i <= gadgetCount; i++)
{
    var gadget = global.GadgetList[i];
    
    if(!array_contains(global.InstructList,gadget))
    {
        while(true)
        {
            var rand = irandom_range(0,instructionCount)
            show_debug_message("random number: "+string(rand));
            if(!array_contains(blacklist,rand))
            {
                show_debug_message("replacing!")
                global.InstructList[rand] = gadget;
                blacklist[numReplaced] = rand;
                numReplaced ++;
                break;
            }
        }
    }
}


//Create instruction objects

for(i = 0; i <= instructionCount; i++)
{       

    xx = 75
    yy = 91
    
    yy += (i * 17)
    if(i >= 24)
    {
        yy = 91 + ((i - 24) * 17)
        xx = 360
    }

    ropselector = instance_create(xx,yy,obj_rop_selector)
    ropselector.instruction = global.InstructList[i];

    ropselector.isGadget = false;
}

//Create gadget objects

for(i = 0; i <= gadgetCount; i++)
{       

    xx = 710
    yy = 91+(i*17)   

    ropselector = instance_create(xx,yy,obj_rop_selector)
    ropselector.instruction = global.GadgetList[i];

    ropselector.isGadget = true;
}


