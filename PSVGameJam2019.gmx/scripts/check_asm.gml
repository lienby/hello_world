///check_asm()

numGadgets = 23;
numIntructs = 48;

numPossible = 0

for(i = 0; i < numGadgets; i++)
{
    isPossible = false;   
    for(ii = 0; ii < numIntructs; ii++)
    {

       if(global.GadgetList[i] == global.InstructList[ii])
       {
           show_debug_message("MATCH FOUND: IL "+string(ii)+"-"+string(global.InstructList[ii])+" == GL " + string(i) + "-" +string(global.GadgetList[i]))
           isPossible = true;
       }
    }
    if(!isPossible)
    {
        show_debug_message("Not possible! restarting");
        return false;
    }
}

return true;


