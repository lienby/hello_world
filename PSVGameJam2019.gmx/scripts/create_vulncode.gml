///create_vulncode(moduleI,functionI)
var moduleIndex = argument0
var functionIndex = argument1

var f1 = global.kmodFunctionAssembly[moduleIndex]
var farray = f1[functionIndex]

instance_destroy(obj_select_kernel_vulncode);

var len = array_length_1d(farray) -1;
var len2 = array_length_1d(global.kmodVulnCode) -1;

iii = 0

for(var i = 0; i <= len; i++)
{
    for(var ii = 0; ii <= len2; ii++)
    {
        if(farray[i] == global.kmodVulnCode[ii])
        {
            var obj = instance_create(335,337+(16*iii),(obj_select_kernel_vulncode));
            obj.vulnasm = farray[i];
            iii++;
            if(array_contains(global.doneVulnCode, farray[i]))
            {
                obj.done = true;
            }
        }
    }
}

if(!instance_exists(obj_select_kernel_vulncode))
{
    var obj = instance_create(335,337+(16*iii),(obj_select_kernel_vulncode));
    obj.vulnasm = "None found."
    obj.done = true;
}
