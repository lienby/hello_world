///array_contains(array, value)

array = argument0;
value = argument1;


len = array_length_1d(array);

for(var i = 0; i < len; i++)
{
    if(array[i] == value)
    {
        return true;
    }
}

return false;
