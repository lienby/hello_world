///soundloop(snd, allow_overlap)

var snd = argument[0]
if(argument_count >= 2)
{
    var allow_overlap = argument[1]
}
else
{
    var allow_overlap = false;
}

if(!audio_is_playing(snd) || allow_overlap)
    {
    var snd2 = audio_play_sound(snd,0,true);
    audio_sound_gain(snd2, 3, 0);
    }


