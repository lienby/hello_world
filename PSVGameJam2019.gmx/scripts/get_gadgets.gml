///GetProgramCode(instructionCount)

// Some ARM Instructions :D
Memonics[0] = "ADD"
Memonics[1] = "CMP"
Memonics[2] = "BL"
Memonics[3] = "MVN"
Memonics[4] = "CDP"
Memonics[5] = "BX"
Memonics[6] = "BIC"
Memonics[7] = "ADC"
Memonics[8] = "EOR"
Memonics[9] = "LDC"
Memonics[10] = "ORR"
Memonics[11] = "MUL"
Memonics[12] = "CMN"
Memonics[13] = "LDM"
Memonics[14] = "LDR"
Memonics[15] = "MLA"


Registers[0] = "R0"
Registers[1] = "FP"
Registers[2] = "LR"
Registers[3] = "SP"
Registers[4] = "PC"
Registers[5] = "CPSR"


for(i = 0; i <= argument0; i++)
{
    ran1 = random_range(0,15);
    ran2 = random_range(0,5);
    
    fullGadget = Memonics[ran1] +" "+Registers[ran2]+"#";
    ropselector = instance_create(710,96+(i*16),obj_rop_selector)
    ropselector.instruction = fullGadget;
}

return fullGadget;
