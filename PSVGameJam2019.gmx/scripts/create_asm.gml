///create_asm(moduleI,functionI)
var moduleIndex = argument0
var functionIndex = argument1

var f1 = global.kmodFunctionAssembly[moduleIndex]
var farray = f1[functionIndex]

instance_destroy(obj_select_kernel_assembly);

var len = array_length_1d(farray) -1;

for(i2 = 0; i2 <= len; i2++)
{
    var obj = instance_create(653,65+(16*i2),obj_select_kernel_assembly);
    obj.asm = farray[i2];
}
