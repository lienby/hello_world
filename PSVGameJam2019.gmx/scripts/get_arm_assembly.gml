///get_arm_assembly()

// Some ARM Instructions :D
Memonics[0] = "ADC"
Memonics[1] = "ADD"
Memonics[2] = "AND"
Memonics[3] = "B"
Memonics[4] = "BIC"
Memonics[5] = "BL"
Memonics[6] = "BX"
Memonics[7] = "CDP"
Memonics[8] = "CMN"
Memonics[9] = "CMP"
Memonics[10] = "EOR"
Memonics[11] = "LDC"
Memonics[12] = "LDM"
Memonics[13] = "LDR"
Memonics[14] = "MCR"
Memonics[15] = "MLA"
Memonics[16] = "MOV"
Memonics[17] = "MRC"
Memonics[18] = "MRS"
Memonics[19] = "MSR"
Memonics[20] = "MUL"
Memonics[21] = "MVN"
Memonics[22] = "ORR"
Memonics[23] = "RSB"
Memonics[24] = "RSC"
Memonics[25] = "SBC"
Memonics[26] = "STC"
Memonics[27] = "STM"
Memonics[28] = "STR"
Memonics[29] = "SUB"
Memonics[30] = "SWI"
Memonics[31] = "SWP"
Memonics[32] = "TEQ"
Memonics[33] = "TST"

//ARM Registers 

Registers[0] = "R0"
Registers[1] = "R1"
Registers[2] = "R2"
Registers[3] = "R3"
Registers[4] = "R4"
Registers[5] = "R5"
Registers[6] = "FP"
Registers[7] = "IP"
Registers[8] = "SP"
Registers[9] = "LR"
Registers[10] = "PC"
Registers[11] = "CSPR"
Registers[12] = "\#0"
Registers[13] = "\#1"
Registers[14] = "\#2"
Registers[15] = "\#3"
Registers[16] = "\#4"
Registers[17] = "\#5"
Registers[18] = "\#6"
Registers[19] = "\#7"
Registers[20] = "\#8"
Registers[21] = "\#9"
Registers[22] = "\#0"
Registers[23] = "R6"
Registers[24] = "R7"
Registers[25] = "R8"
Registers[26] = "R9"
Registers[27] = "R10"


var lenMemonics = array_length_1d(Memonics) -1;
var lenRegisters = array_length_1d(Registers) -1;

var ran1 = irandom_range(0,lenMemonics);
var ran2 = irandom_range(0,lenRegisters);
var asmCode = Memonics[ran1] +" "+Registers[ran2];

if(irandom_range(1,100) >=50) //Determine if it should have 2 arguments
{
    var ran3 = irandom_range(0,lenRegisters);
    asmCode += ", "+Registers[ran3]
}

return asmCode;   
